-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2020 at 08:25 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `matchcrud`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_name`, `created_at`, `updated_at`) VALUES
(1, 'Kolkata', '2020-07-11 03:54:23', '2020-07-11 03:54:23'),
(2, 'Mumbai', '2020-07-11 03:54:23', '2020-07-11 03:54:23'),
(3, 'Delhi', '2020-07-11 03:54:23', '2020-07-11 03:54:23'),
(4, 'Bangalore', '2020-07-11 03:54:23', '2020-07-11 03:54:23'),
(5, 'Chennai', '2020-07-11 03:54:23', '2020-07-11 03:54:23'),
(6, 'Rajasthan', '2020-07-11 03:54:23', '2020-07-11 03:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_one` int(11) NOT NULL,
  `team_two` int(11) NOT NULL,
  `winner` int(11) NOT NULL,
  `match_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`id`, `team_one`, `team_two`, `winner`, `match_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 3, 0, '2020-07-18 10:06:53', '2020-07-10 23:06:59', '2020-07-12 20:35:16', NULL),
(2, 1, 2, 0, '2020-07-14 07:10:22', '2020-07-12 20:10:28', '2020-07-12 20:12:14', NULL),
(9, 1, 2, 0, '2020-07-18 10:46:04', '2020-07-12 23:46:09', '2020-07-12 23:46:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `match_details`
--

CREATE TABLE `match_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `match` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `player` int(11) NOT NULL,
  `run` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `match_details`
--

INSERT INTO `match_details` (`id`, `match`, `team`, `player`, `run`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 13, 53, NULL, '2020-07-12 23:22:58', NULL),
(2, 1, 2, 17, 95, NULL, '2020-07-12 23:22:58', NULL),
(3, 1, 3, 6, 95, NULL, '2020-07-12 23:22:58', NULL),
(4, 1, 3, 16, 26, NULL, '2020-07-12 23:22:58', NULL),
(5, 2, 1, 6, 10, NULL, NULL, NULL),
(6, 2, 1, 6, 20, NULL, NULL, NULL),
(7, 2, 2, 5, 30, NULL, NULL, NULL),
(8, 2, 2, 10, 40, NULL, NULL, NULL),
(9, 9, 1, 7, 10, NULL, NULL, NULL),
(10, 9, 1, 2, 50, NULL, NULL, NULL),
(11, 9, 2, 1, 60, NULL, NULL, NULL),
(12, 9, 2, 43, 70, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL
) ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(2, 'App\\Team', 2, 'logo', '5f092a4354bdb_1', '5f092a4354bdb_1.png', 'image/png', 'public', 2637, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 1, '2020-07-10 21:26:06', '2020-07-10 21:26:15'),
(3, 'App\\Team', 7, 'logo', '5f092aa84027f_2', '5f092aa84027f_2.png', 'image/png', 'public', 14926, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 2, '2020-07-10 21:28:19', '2020-07-10 21:28:20'),
(5, 'App\\Team', 12, 'logo', '5f09395b3476b_2', '5f09395b3476b_2.png', 'image/png', 'public', 14926, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 3, '2020-07-10 22:30:29', '2020-07-10 22:30:37'),
(6, 'App\\Team', 14, 'logo', '5f0939ab64c4f_2', '5f0939ab64c4f_2.png', 'image/png', 'public', 14926, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 4, '2020-07-10 22:31:50', '2020-07-10 22:31:51'),
(7, 'App\\Team', 15, 'logo', '5f0939cc532de_1', '5f0939cc532de_1.png', 'image/png', 'public', 2637, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 5, '2020-07-10 22:32:23', '2020-07-10 22:32:24'),
(8, 'App\\Team', 16, 'logo', '5f0939e441dae_3', '5f0939e441dae_3.png', 'image/png', 'public', 5894, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 6, '2020-07-10 22:32:50', '2020-07-10 22:32:51'),
(9, 'App\\Team', 17, 'logo', '5f093a31cce59_4', '5f093a31cce59_4.png', 'image/png', 'public', 6047, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 7, '2020-07-10 22:34:04', '2020-07-10 22:34:05'),
(10, 'App\\Team', 18, 'logo', '5f093b74001f5_flag-english', '5f093b74001f5_flag-english.png', 'image/png', 'public', 19646, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 8, '2020-07-10 22:39:25', '2020-07-10 22:39:26'),
(11, 'App\\Player', 1, 'image', '5f093f57e8549_flag-uae', '5f093f57e8549_flag-uae.jpg', 'image/gif', 'public', 1678, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 9, '2020-07-10 22:56:09', '2020-07-10 22:56:10'),
(12, 'App\\Player', 1, 'image', '5f093f97b28a2_logo', '5f093f97b28a2_logo.jpg', 'image/jpeg', 'public', 7506, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 10, '2020-07-10 22:57:05', '2020-07-10 22:57:06'),
(13, 'App\\Team', 20, 'logo', '5f0949bb0c980_flag-english', '5f0949bb0c980_flag-english.png', 'image/png', 'public', 19646, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 11, '2020-07-10 23:40:22', '2020-07-10 23:40:27'),
(14, 'App\\Team', 21, 'logo', '5f09565f1f20e_flag-english', '5f09565f1f20e_flag-english.png', 'image/png', 'public', 19646, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 12, '2020-07-11 00:34:17', '2020-07-11 00:34:18'),
(15, 'App\\Team', 22, 'logo', '5f09567e06031_flag-uae', '5f09567e06031_flag-uae.jpg', 'image/gif', 'public', 1678, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 13, '2020-07-11 00:34:48', '2020-07-11 00:34:49'),
(16, 'App\\Team', 1, 'logo', '5f0956ea820c8_flag-english', '5f0956ea820c8_flag-english.png', 'image/png', 'public', 19646, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 14, '2020-07-11 00:36:37', '2020-07-11 00:36:38'),
(17, 'App\\Team', 2, 'logo', '5f095757ece49_canada', '5f095757ece49_canada.jpg', 'image/jpeg', 'public', 48019, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 15, '2020-07-11 00:38:26', '2020-07-11 00:38:27'),
(18, 'App\\Team', 3, 'logo', '5f09579c7b56d_india', '5f09579c7b56d_india.png', 'image/png', 'public', 1157, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 16, '2020-07-11 00:39:38', '2020-07-11 00:39:39'),
(19, 'App\\Team', 4, 'logo', '5f0957fd4652e_srilanka', '5f0957fd4652e_srilanka.png', 'image/png', 'public', 3856, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 17, '2020-07-11 00:41:11', '2020-07-11 00:41:12'),
(20, 'App\\Player', 2, 'image', '5f09595778301_canada', '5f09595778301_canada.jpg', 'image/jpeg', 'public', 48019, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 18, '2020-07-11 00:47:04', '2020-07-11 00:47:05'),
(21, 'App\\Player', 2, 'image', '5f098f0a3cd3a_1', '5f098f0a3cd3a_1.png', 'image/png', 'public', 2637, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 19, '2020-07-11 04:36:05', '2020-07-11 04:36:14'),
(22, 'App\\Player', 2, 'image', '5f098f5c86436_3', '5f098f5c86436_3.png', 'image/png', 'public', 5894, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 20, '2020-07-11 04:37:30', '2020-07-11 04:37:31'),
(23, 'App\\Player', 1, 'image', '5f098fbbb2913_4', '5f098fbbb2913_4.png', 'image/png', 'public', 6047, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 21, '2020-07-11 04:39:02', '2020-07-11 04:39:02'),
(24, 'App\\Player', 2, 'image', '5f0a6b503f2ec_01', '5f0a6b503f2ec_01.jpg', 'image/jpeg', 'public', 38561, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 22, '2020-07-11 20:22:02', '2020-07-11 20:22:13'),
(25, 'App\\Player', 3, 'image', '5f0a6ce7f08cd_02', '5f0a6ce7f08cd_02.jpg', 'image/jpeg', 'public', 5251, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 23, '2020-07-11 20:22:42', '2020-07-11 20:22:42'),
(26, 'App\\Player', 1, 'image', '5f0a6d0bc42da_12', '5f0a6d0bc42da_12.jpg', 'image/jpeg', 'public', 6795, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 24, '2020-07-11 20:23:20', '2020-07-11 20:23:21'),
(27, 'App\\Team', 1, 'logo', '5f0a6d73486d6_ind', '5f0a6d73486d6_ind.jpg', 'image/jpeg', 'public', 1274, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 25, '2020-07-11 20:25:18', '2020-07-11 20:25:18'),
(28, 'App\\Team', 2, 'logo', '5f0a6e6815be4_australia', '5f0a6e6815be4_australia.png', 'image/png', 'public', 1792, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 26, '2020-07-11 20:29:06', '2020-07-11 20:29:06'),
(29, 'App\\Team', 3, 'logo', '5f0a6eb40a991_westindies', '5f0a6eb40a991_westindies.png', 'image/png', 'public', 4303, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 27, '2020-07-11 20:30:21', '2020-07-11 20:30:22'),
(30, 'App\\Player', 4, 'image', '5f0a6eff92abd_04', '5f0a6eff92abd_04.jpg', 'image/jpeg', 'public', 6777, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 28, '2020-07-11 20:31:40', '2020-07-11 20:31:41'),
(31, 'App\\Player', 5, 'image', '5f0a6f0dcaedb_05', '5f0a6f0dcaedb_05.jpg', 'image/jpeg', 'public', 7057, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 29, '2020-07-11 20:31:55', '2020-07-11 20:31:56'),
(32, 'App\\Player', 6, 'image', '5f0a6f1c89e44_07', '5f0a6f1c89e44_07.jpg', 'image/jpeg', 'public', 34601, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 30, '2020-07-11 20:32:06', '2020-07-11 20:32:07'),
(33, 'App\\Player', 7, 'image', '5f0a6f26e2b59_08', '5f0a6f26e2b59_08.jpg', 'image/jpeg', 'public', 7084, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 31, '2020-07-11 20:32:28', '2020-07-11 20:32:28'),
(34, 'App\\Player', 8, 'image', '5f0a6f391b9bb_09', '5f0a6f391b9bb_09.jpg', 'image/jpeg', 'public', 41452, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 32, '2020-07-11 20:32:35', '2020-07-11 20:32:36'),
(35, 'App\\Player', 9, 'image', '5f0a6f407613c_10', '5f0a6f407613c_10.jpg', 'image/jpeg', 'public', 8245, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 33, '2020-07-11 20:32:43', '2020-07-11 20:32:44'),
(36, 'App\\Player', 10, 'image', '5f0a6f7acbc16_13', '5f0a6f7acbc16_13.jpg', 'image/jpeg', 'public', 8335, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 34, '2020-07-11 20:33:49', '2020-07-11 20:33:50'),
(37, 'App\\Player', 11, 'image', '5f0a6f832e672_14', '5f0a6f832e672_14.jpg', 'image/jpeg', 'public', 5953, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 35, '2020-07-11 20:33:53', '2020-07-11 20:33:53'),
(38, 'App\\Player', 13, 'image', '5f0a6f8ea15fc_15', '5f0a6f8ea15fc_15.jpg', 'image/jpeg', 'public', 6190, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 36, '2020-07-11 20:34:01', '2020-07-11 20:34:02'),
(39, 'App\\Player', 14, 'image', '5f0a6f998840f_16', '5f0a6f998840f_16.jpg', 'image/jpeg', 'public', 6963, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 37, '2020-07-11 20:34:14', '2020-07-11 20:34:14'),
(40, 'App\\Player', 12, 'image', '5f0a7026c45b7_17', '5f0a7026c45b7_17.jpg', 'image/jpeg', 'public', 5275, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 38, '2020-07-11 20:36:32', '2020-07-11 20:36:33'),
(41, 'App\\Player', 15, 'image', '5f0a70a976de6_20', '5f0a70a976de6_20.jpg', 'image/jpeg', 'public', 7289, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 39, '2020-07-11 20:39:07', '2020-07-11 20:39:08'),
(42, 'App\\Player', 16, 'image', '5f0a70ce3da3f_21', '5f0a70ce3da3f_21.jpg', 'image/jpeg', 'public', 9382, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 40, '2020-07-11 20:39:35', '2020-07-11 20:39:44'),
(43, 'App\\Player', 17, 'image', '5f0a715ba4969_22', '5f0a715ba4969_22.png', 'image/png', 'public', 255278, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 41, '2020-07-11 20:41:43', '2020-07-11 20:41:46'),
(44, 'App\\Player', 18, 'image', '5f0a717672d13_23', '5f0a717672d13_23.jpg', 'image/jpeg', 'public', 14149, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 42, '2020-07-11 20:42:32', '2020-07-11 20:42:33'),
(45, 'App\\Player', 19, 'image', '5f0a717c75d93_24', '5f0a717c75d93_24.jpg', 'image/jpeg', 'public', 6138, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 43, '2020-07-11 20:42:50', '2020-07-11 20:42:50'),
(46, 'App\\Player', 20, 'image', '5f0a7182e19a3_25', '5f0a7182e19a3_25.png', 'image/png', 'public', 195012, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 44, '2020-07-11 20:42:59', '2020-07-11 20:42:59'),
(47, 'App\\Player', 21, 'image', '5f0a719757c59_26', '5f0a719757c59_26.jpg', 'image/jpeg', 'public', 7202, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 45, '2020-07-11 20:43:06', '2020-07-11 20:43:07'),
(48, 'App\\Player', 22, 'image', '5f0a71ec932e0_28', '5f0a71ec932e0_28.jpg', 'image/jpeg', 'public', 8109, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 46, '2020-07-11 20:44:24', '2020-07-11 20:44:24'),
(49, 'App\\Player', 23, 'image', '5f0a71f234554_29', '5f0a71f234554_29.jpg', 'image/jpeg', 'public', 8923, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 47, '2020-07-11 20:44:34', '2020-07-11 20:44:34'),
(50, 'App\\Player', 24, 'image', '5f0a71fb47def_30', '5f0a71fb47def_30.jpg', 'image/jpeg', 'public', 499626, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 48, '2020-07-11 20:44:42', '2020-07-11 20:44:44'),
(51, 'App\\Player', 25, 'image', '5f0a7239659e1_31', '5f0a7239659e1_31.jpg', 'image/jpeg', 'public', 7628, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 49, '2020-07-11 20:45:29', '2020-07-11 20:45:29'),
(52, 'App\\Player', 26, 'image', '5f0a723e0c480_32', '5f0a723e0c480_32.jpg', 'image/jpeg', 'public', 50013, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 50, '2020-07-11 20:45:36', '2020-07-11 20:45:37'),
(53, 'App\\Player', 27, 'image', '5f0a726d4c006_33', '5f0a726d4c006_33.jpg', 'image/jpeg', 'public', 51791, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 51, '2020-07-11 20:46:21', '2020-07-11 20:46:22'),
(54, 'App\\Player', 28, 'image', '5f0a727307269_34', '5f0a727307269_34.jpg', 'image/jpeg', 'public', 12059, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 52, '2020-07-11 20:46:30', '2020-07-11 20:46:31'),
(55, 'App\\Player', 29, 'image', '5f0a72d9c45b6_43', '5f0a72d9c45b6_43.jpg', 'image/jpeg', 'public', 10068, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 53, '2020-07-11 20:48:06', '2020-07-11 20:48:06'),
(56, 'App\\Player', 30, 'image', '5f0a72d5ebbae_42', '5f0a72d5ebbae_42.jpg', 'image/jpeg', 'public', 9742, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 54, '2020-07-11 20:48:10', '2020-07-11 20:48:10'),
(57, 'App\\Player', 31, 'image', '5f0a72d05e3b3_41', '5f0a72d05e3b3_41.jpg', 'image/jpeg', 'public', 7481, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 55, '2020-07-11 20:48:15', '2020-07-11 20:48:16'),
(58, 'App\\Player', 32, 'image', '5f0a72ca5f6a6_40', '5f0a72ca5f6a6_40.jpg', 'image/jpeg', 'public', 7597, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 56, '2020-07-11 20:48:25', '2020-07-11 20:48:26'),
(59, 'App\\Player', 33, 'image', '5f0a732742a7b_47', '5f0a732742a7b_47.jpg', 'image/jpeg', 'public', 1036496, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 57, '2020-07-11 20:49:23', '2020-07-11 20:49:25'),
(60, 'App\\Player', 34, 'image', '5f0a7322c0e7a_46', '5f0a7322c0e7a_46.jpg', 'image/jpeg', 'public', 32159, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 58, '2020-07-11 20:49:29', '2020-07-11 20:49:30'),
(61, 'App\\Player', 35, 'image', '5f0a731deb5de_45', '5f0a731deb5de_45.jpg', 'image/jpeg', 'public', 188101, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 59, '2020-07-11 20:49:36', '2020-07-11 20:49:36'),
(62, 'App\\Player', 36, 'image', '5f0a73174c643_44', '5f0a73174c643_44.jpg', 'image/jpeg', 'public', 9992, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 60, '2020-07-11 20:49:42', '2020-07-11 20:49:43'),
(63, 'App\\Player', 37, 'image', '5f0a7373d3b91_51', '5f0a7373d3b91_51.jpg', 'image/jpeg', 'public', 100106, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 61, '2020-07-11 20:50:43', '2020-07-11 20:50:44'),
(64, 'App\\Player', 38, 'image', '5f0a736de3f86_50', '5f0a736de3f86_50.jpg', 'image/jpeg', 'public', 27279, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 62, '2020-07-11 20:50:48', '2020-07-11 20:50:48'),
(65, 'App\\Player', 39, 'image', '5f0a7367a2b81_49', '5f0a7367a2b81_49.jpg', 'image/jpeg', 'public', 5877, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 63, '2020-07-11 20:50:53', '2020-07-11 20:50:53'),
(66, 'App\\Player', 40, 'image', '5f0a735ff05e3_48', '5f0a735ff05e3_48.jpg', 'image/jpeg', 'public', 22294, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 64, '2020-07-11 20:50:57', '2020-07-11 20:50:58'),
(67, 'App\\Team', 4, 'logo', '5f0a743757fa0_england', '5f0a743757fa0_england.png', 'image/png', 'public', 3690, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 65, '2020-07-11 20:53:54', '2020-07-11 20:53:55'),
(68, 'App\\Player', 41, 'image', '5f0a74f873735_73', '5f0a74f873735_73.jpg', 'image/jpeg', 'public', 8876, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 66, '2020-07-11 20:57:08', '2020-07-11 20:57:09'),
(69, 'App\\Player', 42, 'image', '5f0a755366300_69', '5f0a755366300_69.jpg', 'image/jpeg', 'public', 8262, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 67, '2020-07-11 20:58:41', '2020-07-11 20:58:42'),
(70, 'App\\Player', 43, 'image', '5f0a753408873_70', '5f0a753408873_70.jpg', 'image/jpeg', 'public', 9484, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 68, '2020-07-11 20:58:46', '2020-07-11 20:58:47'),
(71, 'App\\Player', 44, 'image', '5f0a752f868e6_71', '5f0a752f868e6_71.jpg', 'image/jpeg', 'public', 7821, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 69, '2020-07-11 20:58:50', '2020-07-11 20:58:50'),
(72, 'App\\Player', 45, 'image', '5f0a752b9a7a2_72', '5f0a752b9a7a2_72.jpg', 'image/jpeg', 'public', 7609, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 70, '2020-07-11 20:58:56', '2020-07-11 20:58:57'),
(73, 'App\\Player', 46, 'image', '5f0a759c61f1b_65', '5f0a759c61f1b_65.jpg', 'image/jpeg', 'public', 7347, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 71, '2020-07-11 20:59:53', '2020-07-11 20:59:53'),
(74, 'App\\Player', 47, 'image', '5f0a75975c897_66', '5f0a75975c897_66.jpg', 'image/jpeg', 'public', 34066, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 72, '2020-07-11 20:59:58', '2020-07-11 20:59:58'),
(75, 'App\\Player', 48, 'image', '5f0a75927b0bc_67', '5f0a75927b0bc_67.jpg', 'image/jpeg', 'public', 7220, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 73, '2020-07-11 21:00:04', '2020-07-11 21:00:05'),
(76, 'App\\Player', 49, 'image', '5f0a758d58f1c_68', '5f0a758d58f1c_68.jpg', 'image/jpeg', 'public', 6841, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 74, '2020-07-11 21:00:11', '2020-07-11 21:00:12'),
(77, 'App\\Player', 50, 'image', '5f0a75d7b6954_64', '5f0a75d7b6954_64.jpg', 'image/jpeg', 'public', 6841, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 75, '2020-07-11 21:01:17', '2020-07-11 21:01:17'),
(78, 'App\\Player', 51, 'image', '5f0a75de35438_63', '5f0a75de35438_63.jpg', 'image/jpeg', 'public', 5706, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 76, '2020-07-11 21:01:21', '2020-07-11 21:01:22'),
(79, 'App\\Player', 52, 'image', '5f0a75e53b050_62', '5f0a75e53b050_62.jpg', 'image/jpeg', 'public', 108688, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 77, '2020-07-11 21:01:27', '2020-07-11 21:01:28'),
(80, 'App\\Player', 53, 'image', '5f0a75ea9dab9_61', '5f0a75ea9dab9_61.jpg', 'image/jpeg', 'public', 36656, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 78, '2020-07-11 21:01:33', '2020-07-11 21:01:34'),
(81, 'App\\Player', 54, 'image', '5f0a760c2a589_60', '5f0a760c2a589_60.jpg', 'image/jpeg', 'public', 6780, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 79, '2020-07-11 21:01:46', '2020-07-11 21:01:46'),
(82, 'App\\Team', 1, 'logo', '5f0a77b597203_kkr', '5f0a77b597203_kkr.png', 'image/png', 'public', 5004, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 80, '2020-07-11 21:08:49', '2020-07-11 21:08:50'),
(83, 'App\\Team', 2, 'logo', '5f0a77fbc8b8f_mumbai_indians', '5f0a77fbc8b8f_mumbai_indians.png', 'image/png', 'public', 5755, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 81, '2020-07-11 21:10:06', '2020-07-11 21:10:07'),
(84, 'App\\Team', 4, 'logo', '5f0a783272e16_rcb', '5f0a783272e16_rcb.jpg', 'image/jpeg', 'public', 5484, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 82, '2020-07-11 21:10:59', '2020-07-11 21:11:00'),
(85, 'App\\Team', 3, 'logo', '5f0a786aaa88a_rr', '5f0a786aaa88a_rr.png', 'image/png', 'public', 4080, '[]', '{\"generated_conversions\":{\"thumb\":true}}', '[]', 83, '2020-07-11 21:11:49', '2020-07-11 21:11:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2020_07_09_000001_create_media_table', 1),
(8, '2020_07_09_000002_create_permissions_table', 1),
(9, '2020_07_09_000003_create_roles_table', 1),
(10, '2020_07_09_000004_create_users_table', 1),
(11, '2020_07_09_000005_create_teams_table', 1),
(12, '2020_07_09_000006_create_players_table', 1),
(13, '2020_07_09_000007_create_matches_table', 1),
(14, '2020_07_09_000008_create_player_details_table', 1),
(15, '2020_07_09_000009_create_permission_role_pivot_table', 1),
(16, '2020_07_09_000010_create_role_user_pivot_table', 1),
(17, '2020_07_11_045420_create_countries_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user_management_access', NULL, NULL, NULL),
(2, 'permission_create', NULL, NULL, NULL),
(3, 'permission_edit', NULL, NULL, NULL),
(4, 'permission_show', NULL, NULL, NULL),
(5, 'permission_delete', NULL, NULL, NULL),
(6, 'permission_access', NULL, NULL, NULL),
(7, 'role_create', NULL, NULL, NULL),
(8, 'role_edit', NULL, NULL, NULL),
(9, 'role_show', NULL, NULL, NULL),
(10, 'role_delete', NULL, NULL, NULL),
(11, 'role_access', NULL, NULL, NULL),
(12, 'user_create', NULL, NULL, NULL),
(13, 'user_edit', NULL, NULL, NULL),
(14, 'user_show', NULL, NULL, NULL),
(15, 'user_delete', NULL, NULL, NULL),
(16, 'user_access', NULL, NULL, NULL),
(17, 'team_create', NULL, NULL, NULL),
(18, 'team_edit', NULL, NULL, NULL),
(19, 'team_show', NULL, NULL, NULL),
(20, 'team_delete', NULL, NULL, NULL),
(21, 'team_access', NULL, NULL, NULL),
(22, 'player_create', NULL, NULL, NULL),
(23, 'player_edit', NULL, NULL, NULL),
(24, 'player_show', NULL, NULL, NULL),
(25, 'player_delete', NULL, NULL, NULL),
(26, 'player_access', NULL, NULL, NULL),
(27, 'match_create', NULL, NULL, NULL),
(28, 'match_edit', NULL, NULL, NULL),
(29, 'match_show', NULL, NULL, NULL),
(30, 'match_delete', NULL, NULL, NULL),
(31, 'match_access', NULL, NULL, NULL),
(32, 'player_detail_create', NULL, NULL, NULL),
(33, 'player_detail_edit', NULL, NULL, NULL),
(34, 'player_detail_show', NULL, NULL, NULL),
(35, 'player_detail_delete', NULL, NULL, NULL),
(36, 'player_detail_access', NULL, NULL, NULL),
(37, 'profile_password_edit', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37);

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jersey_number` int(11) NOT NULL,
  `team` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`id`, `first_name`, `last_name`, `jersey_number`, `team`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sourav', 'Gangully', 22, 1, '2020-07-10 22:56:09', '2020-07-11 20:23:20', NULL),
(2, 'Dhoni', 'XY', 7, 1, '2020-07-11 00:47:04', '2020-07-11 20:22:02', NULL),
(3, 'IndPlayer', 'GHX', 75, 1, '2020-07-11 20:22:41', '2020-07-11 20:22:41', NULL),
(4, 'IndPlayer', 'BJV', 30, 1, '2020-07-11 20:31:40', '2020-07-11 20:31:40', NULL),
(5, 'IndPlayer', 'EIR', 530, 1, '2020-07-11 20:31:55', '2020-07-11 20:31:55', NULL),
(6, 'IndPlayer', 'AJY', 660, 1, '2020-07-11 20:32:06', '2020-07-11 20:32:06', NULL),
(7, 'Virat', 'Kholi', 220, 1, '2020-07-11 20:32:27', '2020-07-11 20:32:27', NULL),
(8, 'IndPlayer', 'AFQ', 940, 1, '2020-07-11 20:32:35', '2020-07-11 20:32:35', NULL),
(9, 'IndPlayer', 'GHS', 580, 1, '2020-07-11 20:32:43', '2020-07-11 20:32:43', NULL),
(10, 'IndPlayer', 'AIN', 410, 1, '2020-07-11 20:33:49', '2020-07-11 20:33:49', NULL),
(11, 'IndPlayer', 'CHQ', 350, 1, '2020-07-11 20:33:52', '2020-07-11 20:33:52', NULL),
(12, 'IndPlayer', 'CHR', 350, 1, '2020-07-11 20:33:55', '2020-07-11 20:36:32', NULL),
(13, 'IndPlayer', 'HNS', 660, 1, '2020-07-11 20:34:01', '2020-07-11 20:34:01', NULL),
(14, 'IndPlayer', 'HKR', 280, 1, '2020-07-11 20:34:13', '2020-07-11 20:34:13', NULL),
(15, 'AusPlayer', 'FHS', 9, 2, '2020-07-11 20:39:07', '2020-07-11 20:39:07', NULL),
(16, 'AusPlayer', 'GIR', 54, 2, '2020-07-11 20:39:34', '2020-07-11 20:39:34', NULL),
(17, 'AusPlayer', 'GMW', 13, 2, '2020-07-11 20:41:43', '2020-07-11 20:41:43', NULL),
(18, 'AusPlayer', 'DMK', 12, 2, '2020-07-11 20:42:32', '2020-07-11 20:42:32', NULL),
(19, 'AusPlayer', 'ENK', 29, 2, '2020-07-11 20:42:49', '2020-07-11 20:42:49', NULL),
(20, 'AusPlayer', 'EGK', 77, 2, '2020-07-11 20:42:59', '2020-07-11 20:42:59', NULL),
(21, 'AusPlayer', 'BFK', 5, 2, '2020-07-11 20:43:06', '2020-07-11 20:43:06', NULL),
(22, 'AusPlayer', 'BIS', 6, 2, '2020-07-11 20:44:24', '2020-07-11 20:44:24', NULL),
(23, 'AusPlayer', 'EJV', 50, 2, '2020-07-11 20:44:34', '2020-07-11 20:44:34', NULL),
(24, 'AusPlayer', 'AGZ', 37, 2, '2020-07-11 20:44:42', '2020-07-11 20:44:42', NULL),
(25, 'AusPlayer', 'BGP', 31, 2, '2020-07-11 20:45:29', '2020-07-11 20:45:29', NULL),
(26, 'AusPlayer', 'GMT', 68, 2, '2020-07-11 20:45:36', '2020-07-11 20:45:36', NULL),
(27, 'AusPlayer', 'EHX', 48, 2, '2020-07-11 20:46:21', '2020-07-11 20:46:21', NULL),
(28, 'AusPlayer', 'HGV', 96, 2, '2020-07-11 20:46:30', '2020-07-11 20:46:30', NULL),
(29, 'WestPlayer', 'GNR', 15, 3, '2020-07-11 20:48:05', '2020-07-11 20:48:05', NULL),
(30, 'WestPlayer', 'HLK', 96, 3, '2020-07-11 20:48:10', '2020-07-11 20:48:10', NULL),
(31, 'WestPlayer', 'EIN', 35, 3, '2020-07-11 20:48:15', '2020-07-11 20:48:15', NULL),
(32, 'WestPlayer', 'DJS', 9, 3, '2020-07-11 20:48:25', '2020-07-11 20:48:25', NULL),
(33, 'WestPlayer', 'DIT', 55, 3, '2020-07-11 20:49:23', '2020-07-11 20:49:23', NULL),
(34, 'WestPlayer', 'DHK', 93, 3, '2020-07-11 20:49:29', '2020-07-11 20:49:29', NULL),
(35, 'WestPlayer', 'AFP', 99, 3, '2020-07-11 20:49:35', '2020-07-11 20:49:35', NULL),
(36, 'WestPlayer', 'HHS', 35, 3, '2020-07-11 20:49:42', '2020-07-11 20:49:42', NULL),
(37, 'WestPlayer', 'AHP', 75, 3, '2020-07-11 20:50:43', '2020-07-11 20:50:43', NULL),
(38, 'WestPlayer', 'HFS', 73, 3, '2020-07-11 20:50:48', '2020-07-11 20:50:48', NULL),
(39, 'WestPlayer', 'EMR', 16, 3, '2020-07-11 20:50:53', '2020-07-11 20:50:53', NULL),
(40, 'WestPlayer', 'FFK', 93, 3, '2020-07-11 20:50:57', '2020-07-11 20:50:57', NULL),
(41, 'EngPlayer', 'ANQ', 48, 4, '2020-07-11 20:57:08', '2020-07-11 20:57:08', NULL),
(42, 'EngPlayer', 'HMQ', 61, 4, '2020-07-11 20:58:41', '2020-07-11 20:58:41', NULL),
(43, 'EngPlayer', 'CNK', 32, 4, '2020-07-11 20:58:46', '2020-07-11 20:58:46', NULL),
(44, 'EngPlayer', 'DGN', 2, 4, '2020-07-11 20:58:49', '2020-07-11 20:58:49', NULL),
(45, 'EngPlayer', 'HIP', 16, 4, '2020-07-11 20:58:56', '2020-07-11 20:58:56', NULL),
(46, 'EngPlayer', 'DJR', 42, 4, '2020-07-11 20:59:53', '2020-07-11 20:59:53', NULL),
(47, 'EngPlayer', 'BJR', 17, 4, '2020-07-11 20:59:58', '2020-07-11 20:59:58', NULL),
(48, 'EngPlayer', 'BFW', 9, 4, '2020-07-11 21:00:04', '2020-07-11 21:00:04', NULL),
(49, 'EngPlayer', 'GLW', 76, 4, '2020-07-11 21:00:11', '2020-07-11 21:00:11', NULL),
(50, 'EngPlayer', 'GIX', 45, 4, '2020-07-11 21:01:16', '2020-07-11 21:01:16', NULL),
(51, 'EngPlayer', 'CIL', 18, 4, '2020-07-11 21:01:21', '2020-07-11 21:01:21', NULL),
(52, 'EngPlayer', 'AJZ', 10, 4, '2020-07-11 21:01:27', '2020-07-11 21:01:27', NULL),
(53, 'EngPlayer', 'HHR', 17, 4, '2020-07-11 21:01:33', '2020-07-11 21:01:33', NULL),
(54, 'EngPlayer', 'BKX', 90, 4, '2020-07-11 21:01:45', '2020-07-11 21:01:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `player_details`
--

CREATE TABLE `player_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `match` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `player` int(11) NOT NULL,
  `run` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'User', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `country`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'KKR', 1, '2020-07-11 00:36:37', '2020-07-11 21:08:49', NULL),
(2, 'Mumbai Indians', 2, '2020-07-11 00:38:26', '2020-07-11 21:10:06', NULL),
(3, 'Rajasthan Royals', 6, '2020-07-11 00:39:38', '2020-07-11 21:11:49', NULL),
(4, 'RCB', 4, '2020-07-11 00:41:11', '2020-07-11 21:10:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$HY4wCaPUSAGHOS1rcnm5aevADRX6o79V5Fm4mCnBK6xPo/7lb.qwi', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `match_details`
--
ALTER TABLE `match_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD KEY `role_id_fk_1801366` (`role_id`),
  ADD KEY `permission_id_fk_1801366` (`permission_id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `player_details`
--
ALTER TABLE `player_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `user_id_fk_1801375` (`user_id`),
  ADD KEY `role_id_fk_1801375` (`role_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `match_details`
--
ALTER TABLE `match_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `player_details`
--
ALTER TABLE `player_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_id_fk_1801366` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_id_fk_1801366` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_id_fk_1801375` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_fk_1801375` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
