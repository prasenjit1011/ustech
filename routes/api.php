<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Teams
    Route::post('teams/media', 'TeamApiController@storeMedia')->name('teams.storeMedia');
    Route::apiResource('teams', 'TeamApiController');

    // Players
    Route::post('players/media', 'PlayersApiController@storeMedia')->name('players.storeMedia');
    Route::apiResource('players', 'PlayersApiController');

    // Matches
    Route::apiResource('matches', 'MatchApiController');

    // Player Details
    Route::apiResource('player-details', 'PlayerDetailsApiController');
});
