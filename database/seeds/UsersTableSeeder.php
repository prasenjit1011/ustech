<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$HY4wCaPUSAGHOS1rcnm5aevADRX6o79V5Fm4mCnBK6xPo/7lb.qwi',
                'remember_token' => null,
            ],
        ];

        User::insert($users);
    }
}
