<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('player_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player');
            $table->integer('match');
            $table->integer('team');
            $table->integer('run');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
