<?php

namespace App\Http\Requests;

use App\PlayerDetail;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdatePlayerDetailRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('player_detail_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'player' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'match'  => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'team'   => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'run'    => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
