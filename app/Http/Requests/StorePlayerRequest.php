<?php

namespace App\Http\Requests;

use App\Player;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StorePlayerRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('player_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'first_name'    => [
                'max:255',
                'required',
            ],
            'last_name'     => [
                'max:255',
                'required',
            ],
            'image'         => [
                /*'required',*/
            ],
            'jersey_number' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'team'          => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
