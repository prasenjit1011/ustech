<?php

namespace App\Http\Controllers\Admin;
use App\Employee;
use Auth;

class HomeController
{
    public function index()
    {
		$uid = Auth::user()->id;
		$page = 'dashboard';
		$data = array();
		$data['totalemployee'] 	= 0;
		$data['totalpending'] 	= 0;
		$data['totalreviewed'] 	= 0;
		$data['totalapproved'] 	= 0;	
		return view('home',compact('data','page'));
    }
}
