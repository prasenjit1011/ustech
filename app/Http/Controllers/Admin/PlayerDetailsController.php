<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPlayerDetailRequest;
use App\Http\Requests\StorePlayerDetailRequest;
use App\Http\Requests\UpdatePlayerDetailRequest;
use App\PlayerDetail;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PlayerDetailsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('player_detail_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $playerDetails = PlayerDetail::all();

        return view('admin.playerDetails.index', compact('playerDetails'));
    }

    public function create()
    {
        abort_if(Gate::denies('player_detail_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.playerDetails.create');
    }

    public function store(StorePlayerDetailRequest $request)
    {
        $playerDetail = PlayerDetail::create($request->all());

        return redirect()->route('admin.player-details.index');
    }

    public function edit(PlayerDetail $playerDetail)
    {
        abort_if(Gate::denies('player_detail_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.playerDetails.edit', compact('playerDetail'));
    }

    public function update(UpdatePlayerDetailRequest $request, PlayerDetail $playerDetail)
    {
        $playerDetail->update($request->all());

        return redirect()->route('admin.player-details.index');
    }

    public function show(PlayerDetail $playerDetail)
    {
        abort_if(Gate::denies('player_detail_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.playerDetails.show', compact('playerDetail'));
    }

    public function destroy(PlayerDetail $playerDetail)
    {
        abort_if(Gate::denies('player_detail_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $playerDetail->delete();

        return back();
    }

    public function massDestroy(MassDestroyPlayerDetailRequest $request)
    {
        PlayerDetail::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
