<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMatchRequest;
use App\Http\Requests\StoreMatchRequest;
use App\Http\Requests\UpdateMatchRequest;
use App\Match;
use App\MatchDetail;
use App\PlayerDetail;
use App\Team;
use App\Player;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MatchController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('match_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $matches = Match::all();

        return view('admin.matches.index', compact('matches'));
    }

    public function create()
    {
        abort_if(Gate::denies('match_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$data['teamlist'] = Team::get()->toArray();
		$data['playerlist'] = Player::get()->toArray();
		//dd($teams);
        return view('admin.matches.create', compact('data'));
    }

    public function store(StoreMatchRequest $request)
    {
        $addDetails = $request->addDetails;
		$team_one = $request->team_one;
		$team_two = $request->team_two;
		$winner = $request->winner;
		
		if($team_one == $team_two){
			return  back()->with('error','Both team can\'t be same.');;
		}
		
		if(!in_array($winner,array(0,$team_one,$team_two))){
			return  back()->with('error','Winner can\'t be other team.');;
		}		
		
		
		
		$match = Match::create($request->all());
		
		if($addDetails){
			return redirect()->route('admin.matches.details', $match->id);
		}
		else{
			return redirect()->route('admin.matches.index');
		}		
    }

    public function edit(Match $match)
    {
        abort_if(Gate::denies('match_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
		$data['match_details'] 	= MatchDetail::where('match',$match->id)->get()->count();		
		$data['teamlist'] 		= Team::get()->toArray();
		
		$data['playerlist'] = Player::get()->toArray();
		//dd($teams);
        return view('admin.matches.edit', compact('match','data'));
    }

    public function update(UpdateMatchRequest $request, Match $match)
    {
		$team_one = $request->team_one;
		$team_two = $request->team_two;
		$winner = $request->winner;		
		if($team_one == $team_two){
			return  back()->with('error','Both team can\'t be same.');;
		}	

		if(!in_array($winner,array(0,$team_one,$team_two))){
			return  back()->with('error','Winner can\'t be other team.');;
		}
		
        $match->update($request->all());
        return redirect()->route('admin.matches.index');
    }

    public function show(Match $match)
    {
        abort_if(Gate::denies('match_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.matches.show', compact('match'));
    }

    public function destroy(Match $match)
    {
        abort_if(Gate::denies('match_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $match->delete();

        return back();
    }

    public function massDestroy(MassDestroyMatchRequest $request)
    {
        Match::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
	
	public function details($matchid){		
		$match 				= Match::where('id',$matchid)->get();		
		foreach($match as $val){
			$teamsOne = $val->teamsOne->name;
			$teamsTwo = $val->teamsTwo->name;
		}

		$match_details 		= MatchDetail::where('match',$matchid)->get()->toArray();
		$playerlist 		= Player::get()->toArray();		
		
		return view('admin.matches.details', compact('match','match_details','playerlist','teamsOne','teamsTwo'));
	}
	
	public function detailsUpdate(Request $request,$matchid){
		$matchDetails 	= $request->matchDetails;
		$playerids = array();
		foreach($matchDetails as $val){
			if(in_array($val['player'],$playerids)){
				return  back()->with('error','Duplicate player not allow.');;
			}
			$playerids[] = $val['player'];
		}
		
		//dd($playerids,$matchDetails);
		
		$type 			= $request->type; 
		if($type == 0){
			foreach($matchDetails as $val){
				$val['match'] = $matchid;
				MatchDetail::insert($val);
			}			
		}
		else{
			foreach($matchDetails as $val){
				$mdid = $val['id'];
				unset($val['id']);
				$val['match'] = $matchid;
				MatchDetail::where('id',$mdid)->update($val);
			}			
		}
		return  back()->with('success','Match details updated successfully.');;
	}
}
