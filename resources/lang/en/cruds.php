<?php

return [
    'userManagement' => [
        'title'          => 'User management',
        'title_singular' => 'User management',
    ],
    'permission'     => [
        'title'          => 'Permissions',
        'title_singular' => 'Permission',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'title'             => 'Title',
            'title_helper'      => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'role'           => [
        'title'          => 'Roles',
        'title_singular' => 'Role',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => '',
            'title'              => 'Title',
            'title_helper'       => '',
            'permissions'        => 'Permissions',
            'permissions_helper' => '',
            'created_at'         => 'Created at',
            'created_at_helper'  => '',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => '',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => '',
        ],
    ],
    'user'           => [
        'title'          => 'Users',
        'title_singular' => 'User',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => '',
            'name'                     => 'Name',
            'name_helper'              => '',
            'email'                    => 'Email',
            'email_helper'             => '',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => '',
            'password'                 => 'Password',
            'password_helper'          => '',
            'roles'                    => 'Roles',
            'roles_helper'             => '',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => '',
            'created_at'               => 'Created at',
            'created_at_helper'        => '',
            'updated_at'               => 'Updated at',
            'updated_at_helper'        => '',
            'deleted_at'               => 'Deleted at',
            'deleted_at_helper'        => '',
        ],
    ],
    'team'           => [
        'title'          => 'Team',
        'title_singular' => 'Team',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'name'              => 'Name',
            'name_helper'       => '',
            'logo'              => 'Logo',
            'logo_helper'       => '',
            'country'           => 'State',
            'country_helper'    => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'player'         => [
        'title'          => 'Players',
        'title_singular' => 'Player',
		'details'		 => 'Previous Record of ',
        'fields'         => [
            'id'                   => 'ID',
            'id_helper'            => '',
            'first_name'           => 'First Name',
            'first_name_helper'    => '',
            'last_name'            => 'Last Name',
            'last_name_helper'     => '',
            'image'                => 'Image',
            'image_helper'         => '',
            'jersey_number'        => 'Jersey Number',
            'jersey_number_helper' => '',
            'team'                 => 'Team',
            'team_helper'          => '',
            'created_at'           => 'Created at',
            'created_at_helper'    => '',
            'updated_at'           => 'Updated at',
            'updated_at_helper'    => '',
            'deleted_at'           => 'Deleted at',
            'deleted_at_helper'    => '',
        ],
    ],
    'match'          => [
        'title'          => 'Match',
        'title_singular' => 'Match',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'team_one'          => '1st Team',
            'team_one_helper'   => '',
            'team_two'          => '2nd Team',
            'team_two_helper'   => '',
            'winner'            => 'Winner',
            'winner_helper'     => '',
            'match_date'        => 'Match Date',
            'match_date_helper' => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
    'playerDetail'   => [
        'title'          => 'Player Details',
        'title_singular' => 'Player Detail',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => '',
            'player'            => 'Player',
            'player_helper'     => '',
            'match'             => 'Match',
            'match_helper'      => '',
            'team'              => 'Team',
            'team_helper'       => '',
            'run'               => 'Run',
            'run_helper'        => '',
            'created_at'        => 'Created at',
            'created_at_helper' => '',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => '',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => '',
        ],
    ],
];
