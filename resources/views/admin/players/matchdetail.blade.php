@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.player.details') }} {{ $player->first_name }} {{ $player->last_name }}

    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Player">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.playerDetail.fields.match') }}
                        </th>
                        <th>
                            {{ trans('cruds.player.fields.team') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.match_date') }}
                        </th>
                        <th>
                            {{ trans('cruds.playerDetail.fields.run') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($matchdetail as $key => $match)
                        <tr data-entry-id="{{ $match->id }}">
                            <td>
								{{ $match->matchDetails->teamsOne->name }} Vs {{ $match->matchDetails->teamsTwo->name }}
                            </td>
							<td>
                                {{ $match->teamDetails->name ?? '' }}
                            </td>
							<td>
                                {{ $match->matchDetails->match_date ?? '' }}
                            </td>
							<td>
                                {{ $match->run ?? '' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
		$.extend(true, $.fn.dataTable.defaults, {
			orderCellsTop: true,
			order: [[ 1, 'desc' ]],
			pageLength: 100,
		});
		let table = $('.datatable-Player:not(.ajaxTable)').DataTable()
		$('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust();
		});

		})

</script>
@endsection