@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.player.title_singular') }} {{ trans('global.list') }}
		
		@can('player_create')
			<a class="btn btn-success btn-sm float-right" href="{{ route('admin.players.create') }}">
				{{ trans('global.add') }} {{ trans('cruds.player.title_singular') }}
			</a>
		@endcan		
		
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Player">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.player.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.player.fields.first_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.player.fields.last_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.player.fields.image') }}
                        </th>
                        <th>
                            {{ trans('cruds.player.fields.jersey_number') }}
                        </th>
                        <th>							
                            {{ trans('cruds.player.fields.team') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($players as $key => $player)
                        <tr data-entry-id="{{ $player->id }}">
                            <td>
                                {{ $player->id ?? '' }}
                            </td>
                            <td>
                                {{ $player->first_name ?? '' }}
                            </td>
                            <td>
                                {{ $player->last_name ?? '' }}
                            </td>
                            <td>
                                @if($player->image)
                                    <a href="{{ $player->image->getUrl() }}" target="_blank">
                                        <img src="{{ $player->image->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $player->jersey_number ?? '' }}
                            </td>
                            <td>
                                {{ $player->teams->name ?? '' }}
                            </td>
                            <td>
                                @can('player_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.players.show', $player->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
									
									
                                @endcan

                                @can('player_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.players.edit', $player->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
								
								<a class="btn btn-xs btn-primary" href="{{ route('admin.players.matchDetails', $player->id) }}">
									{{ trans('global.matchDetails') }}
								</a>
                                @can('player_delete')
                                    <form action="{{ route('admin.players.destroy', $player->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
		$.extend(true, $.fn.dataTable.defaults, {
			orderCellsTop: true,
			order: [[ 1, 'desc' ]],
			pageLength: 100,
		});
		let table = $('.datatable-Player:not(.ajaxTable)').DataTable()
		$('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust();
		});

		})

</script>
@endsection