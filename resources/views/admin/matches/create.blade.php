@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.match.title_singular') }}
    </div>

    <div class="card-body">
		@if ($message = Session::get('error'))
			<div class="alert alert-danger">
				<p>{{ $message }}</p>
			</div>
		@endif	
	
        <form method="POST" action="{{ route("admin.matches.store") }}" enctype="multipart/form-data">
            @csrf
			<input type="hidden" name="addDetails" id="addDetails" value="0" /> 
			<div class="row">			
				<div class="col-md-3 col-sm-6">				
					<div class="form-group">
						<label class="required" for="team_one">{{ trans('cruds.match.fields.team_one') }}</label>
						<select name="team_one" id="team_one" class="form-control">
							@foreach($data['teamlist'] as $val)
								<option value="{{$val['id']}}" @if($val['id'] == old('team_one', '0')) selected @endif>{{$val['name']}}</option>
							@endforeach
						</select>                
						@if($errors->has('team_one'))
							<div class="invalid-feedback">
								{{ $errors->first('team_one') }}
							</div>
						@endif
						<span class="help-block">{{ trans('cruds.match.fields.team_one_helper') }}</span>
					</div>
				</div>					
				<div class="col-md-3 col-sm-6">
			
					<div class="form-group">
						<label class="required" for="team_two">{{ trans('cruds.match.fields.team_two') }}</label>
						<select name="team_two" id="team_two" class="form-control">
							@foreach($data['teamlist'] as $val)
								<option value="{{$val['id']}}" @if($val['id'] == old('team_two', '0')) selected @endif>{{$val['name']}}</option>
							@endforeach
						</select>                
						@if($errors->has('team_two'))
							<div class="invalid-feedback">
								{{ $errors->first('team_two') }}
							</div>
						@endif
						<span class="help-block">{{ trans('cruds.match.fields.team_two_helper') }}</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">			
					<div class="form-group">
						<label class="required" for="match_date">{{ trans('cruds.match.fields.match_date') }}</label>
						<input class="form-control datetime {{ $errors->has('match_date') ? 'is-invalid' : '' }}" type="text" name="match_date" id="match_date" value="{{ old('match_date') }}" required>
						@if($errors->has('match_date'))
							<div class="invalid-feedback">
								{{ $errors->first('match_date') }}
							</div>
						@endif
						<span class="help-block">{{ trans('cruds.match.fields.match_date_helper') }}</span>
					</div>
				</div>	 			
				<div class="col-md-3 col-sm-6">			
					<div class="form-group">
						<label class="" for="winner">{{ trans('cruds.match.fields.winner') }}</label>
						<select name="winner" id="winner" class="form-control">					
							<option value="0">N/A</option>
							@foreach($data['teamlist'] as $val)
								<option value="{{$val['id']}}" @if($val['id'] == old('winner', '0')) selected @endif>{{$val['name']}}</option>
							@endforeach
						</select>                
						@if($errors->has('winner'))
							<div class="invalid-feedback">
								{{ $errors->first('winner') }}
							</div>
						@endif
						<span class="help-block">{{ trans('cruds.match.fields.winner_helper') }}</span>
					</div>
				</div>										
			</div>
			<div class="row">			
				<div class="col-md-3 col-sm-6">
			
					<div class="form-group">
						<button class="btn btn-danger" type="submit" id="sbmt">
							{{ trans('global.save') }}
						</button>
						<button class="btn btn-info" type="button" onclick="addMatchDetails()">
							{{ trans('global.add_details') }}
						</button>					
					</div>
				</div>			
			</div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
 <script>
addMatchDetails = function(){
	$('#addDetails').val(1);
	$('#sbmt').click();
}
</script>  
@endsection