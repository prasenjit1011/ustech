@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.match.title_singular') }} {{ trans('global.list') }}
@can('match_create')
	<a class="btn btn-success btn-sm float-right" href="{{ route('admin.matches.create') }}">
		{{ trans('global.add') }} {{ trans('cruds.match.title_singular') }}
	</a>
@endcan		
    </div>

    <div class="card-body">
	
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Match">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.match.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.team_one') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.team_two') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.winner') }}
                        </th>
                        <th>
                            {{ trans('cruds.match.fields.match_date') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($matches as $key => $match)
                        <tr data-entry-id="{{ $match->id }}">
                            <td>
                                {{ $match->id ?? '' }}
                            </td>
                            <td>
								{{ $match->teamsOne->name }}
                            </td>
                            <td>
                                {{ $match->teamsTwo->name ?? '' }}
                            </td>
                            <td>
                                {{ $match->winnerTeam->name ?? 'N/A' }}
                            </td>
                            <td>
                                {{ $match->match_date ?? '' }}
                            </td>
                            <td>
                                @can('match_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.matches.show', $match->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('match_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.matches.edit', $match->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('match_delete')
                                    <form action="{{ route('admin.matches.destroy', $match->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('match_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.matches.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Match:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection