@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.playerDetail.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.player-details.update", [$playerDetail->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="player">{{ trans('cruds.playerDetail.fields.player') }}</label>
                <input class="form-control {{ $errors->has('player') ? 'is-invalid' : '' }}" type="number" name="player" id="player" value="{{ old('player', $playerDetail->player) }}" step="1" required>
                @if($errors->has('player'))
                    <div class="invalid-feedback">
                        {{ $errors->first('player') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.playerDetail.fields.player_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="match">{{ trans('cruds.playerDetail.fields.match') }}</label>
                <input class="form-control {{ $errors->has('match') ? 'is-invalid' : '' }}" type="number" name="match" id="match" value="{{ old('match', $playerDetail->match) }}" step="1" required>
                @if($errors->has('match'))
                    <div class="invalid-feedback">
                        {{ $errors->first('match') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.playerDetail.fields.match_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="team">{{ trans('cruds.playerDetail.fields.team') }}</label>
                <input class="form-control {{ $errors->has('team') ? 'is-invalid' : '' }}" type="number" name="team" id="team" value="{{ old('team', $playerDetail->team) }}" step="1" required>
                @if($errors->has('team'))
                    <div class="invalid-feedback">
                        {{ $errors->first('team') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.playerDetail.fields.team_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="run">{{ trans('cruds.playerDetail.fields.run') }}</label>
                <input class="form-control {{ $errors->has('run') ? 'is-invalid' : '' }}" type="number" name="run" id="run" value="{{ old('run', $playerDetail->run) }}" step="1" required>
                @if($errors->has('run'))
                    <div class="invalid-feedback">
                        {{ $errors->first('run') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.playerDetail.fields.run_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection