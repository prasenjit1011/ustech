@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.team.title_singular') }} {{ trans('global.list') }}

		@can('team_create')
			<a class="btn btn-success btn-sm float-right" href="{{ route('admin.teams.create') }}">
				{{ trans('global.add') }} {{ trans('cruds.team.title_singular') }}
			</a>
		@endcan

		
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Team">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.team.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.team.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.team.fields.logo') }}
                        </th>
                        <th>
							{{ trans('cruds.team.fields.country') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($teams as $key => $team)
                        <tr data-entry-id="{{ $team->id }}">
                            <td>
                                {{ $team->id ?? '' }}
                            </td>
                            <td>
                                {{ $team->name ?? '' }}
                            </td>
                            <td>
                                @if($team->logo)
                                    <a href="{{ $team->logo->getUrl() }}" target="_blank">
                                        <img src="{{ $team->logo->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
								{{ $team->countries->country_name}}								
                            </td>
                            <td>
                                @can('team_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.teams.show', $team->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('team_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.teams.edit', $team->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('team_delete')
                                    <form action="{{ route('admin.teams.destroy', $team->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
	<script>
		$(function () {
			$.extend(true, $.fn.dataTable.defaults, {
				orderCellsTop: true,
				order: [[ 1, 'desc' ]],
				pageLength: 100,
			});
			let table = $('.datatable-Team:not(.ajaxTable)').DataTable()
			$('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
			$($.fn.dataTable.tables(true)).DataTable()
			.columns.adjust();
			});
		})
	</script>
@endsection