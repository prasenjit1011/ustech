<div class="sidebar">
    <nav class="sidebar-nav">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-fw fa-tachometer-alt">
                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            @can('user_management_access')
                <li class="nav-item nav-dropdown">
                    <a class="nav-link  nav-dropdown-toggle" href="#">
                        <i class="fa-fw fas fa-users nav-icon">

                        </i>
                        {{ trans('cruds.userManagement.title') }}
                    </a>
                    <ul class="nav-dropdown-items">
                        \
						
						
						@can('permission_access')
                            <li class="nav-item">
                                <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                    <i class="fa-fw fas fa-unlock-alt nav-icon"</i>
                                    {{ trans('cruds.permission.title') }}
                                </a>
                            </li>
                        @endcan
                        @can('role_access')
                            <li class="nav-item">
                                <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                    <i data-feather="droplet" class="mr-2"></i>{{ trans('cruds.role.title') }}
                                </a>
                            </li>
                        @endcan
                        @can('user_access')
                            <li class="nav-item">
                                <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                    <i data-feather="droplet" class="mr-2"></i>{{ trans('cruds.user.title') }}
                                </a>
                            </li>
                        @endcan
						
                    </ul>
                </li>
            @endcan

            @can('departments_access')
                <li class="nav-item">
                    <a href="{{ route("admin.departments.index") }}" class="nav-link {{ request()->is('admin/departmentss') || request()->is('admin/departmentss/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fa-cogs nav-icon">

                        </i>
                        {{ trans('cruds.departments.title') }}
                    </a>
                </li>
            @endcan
			
            @can('certificate_access')
                <li class="nav-item">
                    <a href="{{ route("admin.certificates.index") }}" class="nav-link {{ request()->is('admin/certificates') || request()->is('admin/certificates/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fa-cogs nav-icon">

                        </i>
                        {{ trans('cruds.certificate.title') }}
                    </a>
                </li>
            @endcan
			
			
			
			
        <ul class="nav">
			<li class="nav-item nav-dropdown">
				<a class="nav-link  nav-dropdown-toggle" href="#">
					<i class="fa-fw fas fa-users nav-icon"></i>
					{{ trans('global.certificate') }}
				</a>
				<ul class="nav-dropdown-items">
				@can('employee_access')
					<li class="nav-item">
						<a href="{{ route("admin.employees.index") }}" class="nav-link {{ request()->is('admin/employees') || request()->is('admin/employees/*') ? 'active' : '' }}">
							<i class="fa-fw fas fa-cogs nav-icon"></i>
							{{ trans('global.external') }}
						</a>
					</li>
					<li class="nav-item">
						<a href="{{ route("admin.members.index") }}" class="nav-link {{ request()->is('admin/members') || request()->is('admin/members/*') ? 'active' : '' }}">
							<i class="fa-fw fas fa-cogs nav-icon"></i>
							{{ trans('global.members') }}
						</a>
					</li>					
					<li class="nav-item">
						<a href="{{ route("admin.internalemployees.empSearch") }}" class="nav-link {{ request()->is('admin/internalemployees') || request()->is('admin/internalemployees/*') ? 'active' : '' }}">
							<i class="fa-fw fas fa-cogs nav-icon"></i>
							{{ trans('global.internal') }}
						</a>
					</li>
					
				@endcan
				@can('member_access') @endcan

					
					<li class="nav-item d-none1">
						<a href="{{ route("admin.members.all") }}" class="nav-link {{ request()->is('admin/members/all') || request()->is('admin/members/all') ? 'active' : '' }}">
							<i class="fa-fw fas fa-cogs nav-icon"></i>
							{{ trans('global.allmembers') }}
						</a>
					</li>
					<?php /*
					<li class="nav-item">
						<a href="{{ route("admin.employees.listing") }}" class="nav-link {{ request()->is('employees/listing') ? 'active' : '' }}">
							<i class="fa-fw fas fa-cogs nav-icon"></i>
							{{ trans('global.allinternalmembers') }}
						</a>
					</li>*/ ?>
					
					
        @can('internalmember_access')
            <li class="nav-item">
                <a href="{{ route("admin.internalmembers.index") }}" class="nav-link {{ request()->is('admin/internalmembers') || request()->is('admin/internalmembers/*') ? 'active' : '' }}">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon"></i> &nbsp; 
                    {{ trans('cruds.internalmember.title') }}
                </a>
            </li>
        @endcan					
				
				</ul>
            </li>
		</ul>
			
            @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                @can('profile_password_edit')
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                            <i class="fa-fw fas fa-key nav-icon">
                            </i>
                            {{ trans('global.change_password') }}
                        </a>
                    </li>
                @endcan
            @endif
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-fw fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
        </ul>

    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>