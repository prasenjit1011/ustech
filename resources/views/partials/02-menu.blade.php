<aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="mt-3">
                            <a href="{{ route("admin.home") }}" class="btn create-btn text-white no-wrap d-flex align-items-center">
                                <i data-feather="plus"></i>
                                <span class="hide-menu ml-2 text-uppercase">{{ trans('global.dashboard') }}</span>
                            </a>
                        </li>
						@can('user_management_access')
							<li class="sidebar-item">
								<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
									aria-expanded="false">
									<i data-feather="home" class="mr-2"></i>
									<span class="hide-menu">{{ trans('cruds.userManagement.title') }}</span>
								</a>
								<ul aria-expanded="false" class="collapse  first-level">
									
									@can('permission_access')
										<li class="sidebar-item">
											<a href="{{ route("admin.permissions.index") }}" class="sidebar-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
												<i data-feather="droplet" class="mr-2"></i>
												<span class="hide-menu">{{ trans('cruds.permission.title') }}</span>
											</a>
										</li>
									@endcan
									@can('role_access')
										<li class="sidebar-item">
											<a href="{{ route("admin.roles.index") }}" class="sidebar-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
												<i data-feather="droplet" class="mr-2"></i>
												<span class="hide-menu">{{ trans('cruds.role.title') }}</span>
											</a>
										</li>
									@endcan
									@can('user_access')
										<li class="sidebar-item">
											<a href="{{ route("admin.users.index") }}" class="sidebar-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
												<i data-feather="droplet" class="mr-2"></i>
												<span class="hide-menu">{{ trans('cruds.user.title') }}</span>
											</a>
										</li>
									@endcan	
								</ul>
							</li>
						@endcan
						
						
            @can('department_access')
                <li class="sidebar-item">
                    <a href="{{ route("admin.departments.index") }}" class="sidebar-link {{ request()->is('admin/departmentss') || request()->is('admin/departmentss/*') ? 'active' : '' }}">
                        <i data-feather="home" class="mr-2"></i>
                        <span class="hide-menu">{{ trans('cruds.departments.title') }}</span>
                    </a>
                </li>
            @endcan
			
            @can('certificate_access')
                <li class="sidebar-item">
                    <a href="{{ route("admin.certificates.index") }}" class="sidebar-link {{ request()->is('admin/certificates') || request()->is('admin/certificates/*') ? 'active' : '' }}">
                        <i data-feather="home" class="mr-2"></i>
                        <span class="hide-menu">{{ trans('cruds.certificate.title') }}</span>
                    </a>
                </li>
            @endcan						
						
						
						@can('user_management_access')
							<li class="sidebar-item">
								<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
									aria-expanded="false">
									<i data-feather="home" class="mr-2"></i>
									<span class="hide-menu">{{ trans('global.certificate') }}</span>
								</a>
								<ul aria-expanded="false" class="collapse  first-level">
										
									
				@can('employee_access')
					<li class="sidebar-item">
						<a href="{{ route("admin.employees.index") }}" class="sidebar-link {{ request()->is('admin/employees') || request()->is('admin/employees/*') ? 'active' : '' }}">
							<i data-feather="droplet" class="mr-2"></i>
							<span class="hide-menu">{{ trans('global.external') }}</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="{{ route("admin.members.index") }}" class="sidebar-link {{ request()->is('admin/members') || request()->is('admin/members/*') ? 'active' : '' }}">
							<i data-feather="droplet" class="mr-2"></i>
							<span class="hide-menu">{{ trans('global.members') }}</span>
						</a>
					</li>					
					<li class="sidebar-item">
						<a href="{{ route("admin.internalemployees.empSearch") }}" class="sidebar-link {{ request()->is('admin/internalemployees') || request()->is('admin/internalemployees/*') ? 'active' : '' }}">
							<i data-feather="droplet" class="mr-2"></i>
							<span class="hide-menu">{{ trans('global.internal') }}</span>
						</a>
					</li>
					
					
					<li class="sidebar-item d-none">
						<a href="{{ route("admin.members.all") }}" class="sidebar-link {{ request()->is('admin/members/all') || request()->is('admin/members/all') ? 'active' : '' }}">
							<i data-feather="droplet" class="mr-2"></i>
							<span class="hide-menu">{{ trans('global.allmembers') }}</span>
						</a>
					</li>
        @can('internalmember_access')
            <li class="sidebar-item">
                <a href="{{ route("admin.internalmembers.index") }}" class="sidebar-link {{ request()->is('admin/internalmembers') || request()->is('admin/internalmembers/*') ? 'active' : '' }}">
                    <i data-feather="droplet" class="mr-2"></i> 
                    <span class="hide-menu">{{ trans('cruds.internalmember.title') }}</span>
                </a>
            </li>
        @endcan						
				@endcan									
									
								</ul>
							</li>
						@endcan
						
            @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                @can('profile_password_edit')
                    <li class="sidebar-item">
                        <a class="sidebar-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                            <i data-feather="home" class="mr-2"></i>
                            <span class="hide-menu">{{ trans('global.change_password') }}</span>
                        </a>
                    </li>
					<li class="sidebar-item">
						<a  class="sidebar-link" href="javascript:void(0)"  onclick="lgfrm()">
							<i data-feather="home" class="mr-2"></i>
							<span class="hide-menu">{{ trans('global.logout') }}</span>
						</a>
					</li>					
                @endcan
            @endif


			
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        